## 湖南农业大学17生物信息一班谭强本科毕设
## 我的本科毕设_土壤扩增子测序数据分析流程

#### 基本架构
使用conda管理运行环境（QIIME2环境与R4.0环境）
基于QIIME2生成代表序列，特征表，进化树，注释信息这几个文件
后续使用R脚本进行分析

#### 环境部署

安装conda，管理qiime2与R4.0
```
wget -c https://mirrors.tuna.tsinghua.edu.cn/anaconda/miniconda/Miniconda3-py39_4.9.2-Linux-x86_64.sh 
bash Miniconda3-py39_4.9.2-Linux-x86_64.sh
source ~/.bashrc
```
qiime2配置
```
wget https://data.qiime2.org/distro/core/qiime2-2021.2-py36-linux-conda.yml
conda env create -n qiime2-2021.2 --file qiime2-2021.2-py36-linux-conda.yml
```

R及R包配置
使用conda部署r4.0环境，同时也可用于安装r需要的包（比较稳定）

```
conda create -n R4.0 #创建
conda activate R4.0 #进入
conda search  r #搜索
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge/ #可能需要配置镜像
conda install r=4.0 #安装r4.0（如果有其它更高级版本也可）
```
以下为安装R包
```
conda install -c bioconda bioconductor-ggtree
conda install -c conda-forge r-tidyverse
conda install -c conda-forge r-plotrix
conda install -c r r-devtools #有的包在github，则必须此工具
R #进入R中，用devtools 
library(devtools)
devtools::install_github("jbisanz/qiime2R")
.....其它包安装不再细述
```

#### 使用说明
```
-d 数据库选择，可选：
silva_v4（暂只部署了silva_v4做示例）
###################################################################################
-F
EMP协议的格式时，接三个文件所在目录（自动检测文件数量，选择单双端）
单端必须命名为 ：barcodes.fastq.gz sequences.fastq.gz 
双端必须命名为 ：barcodes.fastq.gz  forward.fastq.gz  reverse.fastq.gz
-----------------------------------------------------------------
fastq清单格式时，接清单文件（自动检测文件列数，选择单双端）（详细见QIIME2官网）
单端表头：sample-id	absolute-filepath
双端表头：sample-id	forward-absolute-filepath	reverse-absolute-filepath
###################################################################################
-m  样品名,(对应标签),对应分组,环境因子
EMP协议时样品名称对应barcode，示例格式：
sampleid    barcode group   env_fac1    env_fac2
demo_name1  DEMO_BARCODE1   A   0.2 1
demo_name2  DEMO_BARCODE2   B   0.4 0.6
----------------------------------------------------------------
fastq清单格式，示例格式
sampleid	group	env_fac1	env_fac2
demo_name1	A	0.2	1
demo_name2	B	0.4	0.6
###################################################################################
-t 可选 fasttree raxml（选择建树方案，默认fasttree）
###################################################################################
-p 引物信息，没有则不去除
双端时 正反引物以下划线连接，如 AGCTAGG_ATGGGT
单端时 则只有一条引物，如 AAAGGGTTT
###################################################################################
-r 根据注释，去除线粒体，叶绿体，可选：
C 去叶绿体
M 去线粒体
CM 去线粒体和叶绿体
```

#### 注意事项
使用conda管理环境时，在shell脚本中使用
```
conda activate env_name
```
时会出现无法找到环境的问题，尚未弄清此问题出现的原因，但可以使用
```
source activate  安装该软件的绝对路径
```
避免此问题，如
```
source activate /PERSONALBIO196/Mb_std/miniconda3/envs/qiime2-2021.2 
```
为避免学术纠纷，请勿在2021年5月22号前作学术用途