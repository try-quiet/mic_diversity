#示例数据下载地址
https://qiita.ucsd.edu/download_study_bioms/10360

本流程的示例数据来自文章 

Neilson J W , Califf K , Cardona C , et al. Significant Impacts of Increasing Aridity on the Arid Soil Microbiome[J]. Msystems, 2017, 2(3):e00195-16.

仅取其中部分数据作为输入，即demo.tsv部分。metadata_bk.tsv为该文章作者提供的完整的序列文件相关信息（FASTQ文件仍需要从上网址下载，并通过解码后使用）

