#conda activate qiime2-2021.2
source activate  /PERSONALBIO196/Mb_std/miniconda3/envs/qiime2-2021.2
tree_type=fasttree
help(){ 
cat << HELP 
-d 数据库选择，可选：
silva_v4（暂只部署了silva_v4做示例）
###################################################################################
-F
EMP协议的格式时，接三个文件所在目录（自动检测文件数量，选择单双端）
单端必须命名为 ：barcodes.fastq.gz sequences.fastq.gz 
双端必须命名为 ：barcodes.fastq.gz  forward.fastq.gz  reverse.fastq.gz
-----------------------------------------------------------------
fastq清单格式时，接清单文件（自动检测文件列数，选择单双端）（详细见QIIME2官网）
单端表头：sample-id	absolute-filepath
双端表头：sample-id	forward-absolute-filepath	reverse-absolute-filepath
###################################################################################
-m  样品名,(对应标签),对应分组,环境因子
EMP协议时样品名称对应barcode，示例格式：
sampleid    barcode group   env_fac1    env_fac2
demo_name1  DEMO_BARCODE1   A   0.2 1
demo_name2  DEMO_BARCODE2   B   0.4 0.6
----------------------------------------------------------------
fastq清单格式，示例格式
sampleid	group	env_fac1	env_fac2
demo_name1	A	0.2	1
demo_name2	B	0.4	0.6
###################################################################################
-t 可选 fasttree raxml（选择建树方案，默认fasttree）
###################################################################################
-p 引物信息，没有则不去除
双端时 正反引物以下划线连接，如 AGCTAGG_ATGGGT
单端时 则只有一条引物，如 AAAGGGTTT
###################################################################################
-r 根据注释，去除线粒体，叶绿体，可选：
C 去叶绿体
M 去线粒体
CM 去线粒体和叶绿体

HELP
exit 0
} 
function paired_rm_primer (){
    if [ -z $3 ];then
        cp $1  $2
    else
        primer_f=`echo $3|cut -d'_' -f1`
        primer_r=`echo $3|cut -d'_' -f2`
        qiime cutadapt trim-paired   --p-front-f  $primer_f  --p-front-r  $primer_r \
        --i-demultiplexed-sequences  $1 \
        --o-trimmed-sequences $2 \
        --p-discard-untrimmed 
    fi    
}
function single_rm_primer () {
    if [ -z $3 ];then
        cp $1  $2
    else
        qiime cutadapt trim-single   --p-front $primer  \
        --i-demultiplexed-sequences  $1 \
        --o-trimmed-sequences $2 \
        --p-discard-untrimmed
    fi
    
}
function paired_table(){
    qiime tools export --input-path $1  --output-path sequences/demux_qza #计算阙值。将长度分布5%的部分序列去除
    trunc_f=`ls sequences/demux_qza/*_R1* | xargs -i zcat {} | awk 'NR%4==2{print length}' | sort -n| uniq -c |awk '{a[NR]=$2;d[$2]=$1;b+=$1}END{for(i=1;i<=NR;i++){c+=d[a[i]];if(c/b>0.02){print a[i];break} }}'`
    trunc_r=`ls sequences/demux_qza/*_R2* | xargs -i zcat {} | awk 'NR%4==2{print length}' | sort -n| uniq -c |awk '{a[NR]=$2;d[$2]=$1;b+=$1}END{for(i=1;i<=NR;i++){c+=d[a[i]];if(c/b>0.02){print a[i];break} }}'`
        #去噪生成代表序列,特征表，统计
    qiime dada2 denoise-paired   --i-demultiplexed-seqs $1 \
        --p-pooling-method "pseudo" --p-chimera-method "pooled" \
        --p-trunc-len-f $trunc_f  --p-trunc-len-r $trunc_r  --p-n-threads 0     \
        --o-table $2/table.qza  \
        --o-representative-sequences $2/rep_seqs.qza   \
        --o-denoising-stats sequences/denoising-stats.qza
    rm  -r sequences/demux_qza
}
function single_table(){
    qiime tools export --input-path $1  --output-path sequences/demux_qza #计算阙值。将长度分布5%的部分序列去除
    trunc_one=`ls sequences/demux_qza/*gz | xargs -i zcat {} | awk 'NR%4==2{print length}' | sort -n| uniq -c |awk '{a[NR]=$2;d[$2]=$1;b+=$1}END{for(i=1;i<=NR;i++){c+=d[a[i]];if(c/b>0.02){print a[i];break} }}'`
    qiime dada2 denoise-single  --i-demultiplexed-seqs $1 \
        --p-pooling-method "pseudo"  --p-chimera-method "pooled" \
        --p-trunc-len $trunc_one  --p-n-threads 0  --o-table $2/table.qza \
        --o-representative-sequences $2/rep_seqs.qza  \
        --o-denoising-stats sequences/denoising-stats.qza
    rm  -r sequences/demux_qza

}



if [[ $# == 0 || $1 == "-h" || $1 == "--help" ]];then help;fi

while getopts ':d:F:m:t:p:r:' OPT;do
    case "$OPT" in
    d)db="$OPTARG";;
    F)input_d_f="$OPTARG";;
    m)meta_d="$OPTARG";;
    t)tree_type="$OPTARG";;
    p)primer="$OPTARG";;
    r)rm_tax="$OPTARG";;
    ?) help;;
    esac
done
#检测输入
declare -A db_dic
db_dic=([silva_v4]="/PERSONALBIO196/Mb_std/tq_graduation/class_db/silva_138_99_515-806_classifier.qza")  #数据库字典

db_file=${db_dic[$db]}
if [ "$db_file" == "" ];then echo "数据库名称不正确" ;exit;fi
if [ "$tree_type" != fasttree -a "$tree_type" != raxml ];then echo"-t fasttree or raxml";help;exit ;fi
if [ ! -f "$meta_d" ];then exit;fi
if [ -n "$rm_tax" -a "$rm_tax"!="C" -a "$rm_tax"!="M" -a  "$rm_tax"!="CM" ];then echo "去除线粒体叶绿体选项错误";help;exit ;fi

mkdir sequences tables_reps tree_taxonomy
if [ -d "$input_d_f" ];then #demux
    if [ `ls $input_d_f|wc -l` -eq 3 ];then
        if [ -n "$primer" ]&&[[ ! $primer =~ "_" ]];then echo "引物类型与序列类型不符";exit;fi
        qiime tools import --type EMPPairedEndSequences --input-path  $input_d_f --output-path sequences/emp-end-sequences.qza
        qiime demux emp-paired \
        --m-barcodes-file $meta_d \
        --m-barcodes-column barcode \
        --p-rev-comp-mapping-barcodes \
        --i-seqs sequences/emp-end-sequences.qza \
        --o-per-sample-sequences sequences/demux.qza \
        --o-error-correction-details sequences/demux-details.qza
        
        paired_rm_primer  sequences/demux.qza sequences/demux_no_primer.qza $primer
        paired_table sequences/demux_no_primer.qza  tables_reps
       
    elif [ `ls $input_d_f|wc -l` -eq 2 ];then
        if [ -n "$primer" ]&&[[ $primer =~ "_" ]];then echo "引物类型与序列类型不符";exit;fi
        qiime tools import --type  EMPSingleEndSequences --input-path  $input_d_f --output-path sequences/emp-end-sequences.qza
        qiime demux emp-single \
        --i-seqs sequences/emp-end-sequences.qza \
        --m-barcodes-file  $meta_d  \
        --m-barcodes-column barcode \
        --p-rev-comp-mapping-barcodes \
        --o-per-sample-sequences sequences/demux.qza \
        --o-error-correction-details sequences/demux-details.qza      
        
        single_rm_primer  sequences/demux.qza sequences/demux_no_primer.qza $primer
        single_table sequences/demux_no_primer.qza  tables_reps
    else
        echo "输入格式有误"
        exit
    fi
elif [ -f "$input_d_f" ];then

    clo=`head -n1 $input_d_f | awk '{print NF}'` 
    if [ $clo -eq 3 ];then #双端
        if [ -n "$primer" ]&&[[ ! $primer =~ "_" ]];then echo "引物类型与序列类型不符";exit;fi

        qiime tools import   --type 'SampleData[PairedEndSequencesWithQuality]' \
        --input-path   $input_d_f  \
        --output-path sequences/demux.qza 
      
        paired_rm_primer  sequences/demux.qza sequences/demux_no_primer.qza $primer
        paired_table sequences/demux_no_primer.qza  tables_reps
    elif [ $clo -eq 2 ];then #单端
        if [ -n "$primer" ]&&[[ $primer =~ "_" ]];then echo "引物类型与序列类型不符";exit;fi

        qiime tools import --type 'SampleData[SequencesWithQuality]'
        --input-path   $input_d_f  \
        --output-path sequences/demux.qza
        single_rm_primer  sequences/demux.qza sequences/demux_no_primer.qza $primer
        single_table sequences/demux_no_primer.qza  tables_reps
    else
        echo "输入格式有误"
        exit
    fi

else
    echo "-F后不存在文件或文件夹"
    help
    exit
fi

qiime tools export --input-path sequences/denoising-stats.qza --output-path sequences/
sed -i '/#/d'  sequences/stats.tsv

qiime feature-table filter-features --i-table tables_reps/table.qza    --p-min-frequency 2       --p-min-samples 1       --o-filtered-table tables_reps/table_no_single.qza
qiime feature-table filter-seqs --i-data tables_reps/rep_seqs.qza    --i-table tables_reps/table_no_single.qza      --o-filtered-data tables_reps/rep_seqs_no_single.qza

qiime feature-classifier classify-sklearn    --p-n-jobs -1 --i-classifier $db_file  --i-reads tables_reps/rep_seqs_no_single.qza --o-classification tree_taxonomy/taxonomy_no_single.qza
if [ -z  $rm_tax ];then
    cp tables_reps/table_no_single.qza  tables_reps/table_filter.qza
    cp tables_reps/rep_seqs_no_single.qza tables_reps/rep_seqs_filter.qza
else 
    if [ $rm_tax=="C" ];then
        re_qiime_str=chloroplast
    elif [ $rm_tax=="M" ];then
        re_qiime_str=mitochondria
    elif [ $rm_tax=="CM" ];then
        re_qiime_str=mitochondria,chloroplast
    fi
    qiime taxa filter-table \
        --i-table tables_reps/table_no_single.qza \
        --i-taxonomy tree_taxonomy/taxonomy_no_single.qza \
        --p-exclude $re_qiime_str \
        --o-filtered-table tables_reps/table_filter.qza

    qiime feature-table filter-seqs --i-data tables_reps/rep_seqs_no_single.qza  \
        --i-table tables_reps/table_filter.qza   \
        --o-filtered-data tables_reps/rep_seqs_filter.qza
fi
#提取代表序列哈希码，与ASV对应
qiime tools export --input-path tables_reps/rep_seqs_filter.qza --output-path tables_reps/rep_tmp
awk '/>/{print substr($0,2)"\tASV_"(NR+1)/2}' tables_reps/rep_tmp/dna-sequences.fasta > tables_reps/hash_ASV.tsv
#改代表序列
awk '(NR==FNR){a[$1]=$2}(NR>FNR){if(a[substr($0,2)]!=""){print">"a[substr($0,2)]}else{print$0}}'  tables_reps/hash_ASV.tsv tables_reps/rep_tmp/dna-sequences.fasta>tables_reps/rep_seqs_filter.fasta
qiime tools import --input-path tables_reps/rep_seqs_filter.fasta --type 'FeatureData[Sequence]' --output-path tables_reps/rep_seqs_filter.qza
#改特征表
qiime tools export --input-path tables_reps/table_filter.qza --output-path tables_reps/table_tmp
biom convert --to-tsv -i tables_reps/table_tmp/feature-table.biom  -o tables_reps/table_tmp/table_tmp.xls --table-type="OTU table"
awk -F'\t' -v OFS='\t' '(NR==FNR){a[$1]=$2}(NR>FNR){ if(a[$1]!=""){$1=a[$1]}; print$0}'  tables_reps/hash_ASV.tsv  tables_reps/table_tmp/table_tmp.xls > tables_reps/table_filter.xls
biom convert -i tables_reps/table_filter.xls -o tables_reps/table_filter.biom --table-type="OTU table" --to-hdf5
qiime tools import --input-path tables_reps/table_filter.biom --type 'FeatureTable[Frequency]'  --output-path tables_reps/table_filter.qza
#改taxonomy_并提取与table.xls对应
qiime tools export --input-path tree_taxonomy/taxonomy_no_single.qza --output-path tree_taxonomy/taxonomy_no_single
awk -F'\t' -v OFS='\t' '(NR==FNR){a[$1]=$2}(NR>FNR){print$2,a[$1]}' tree_taxonomy/taxonomy_no_single/taxonomy.tsv tables_reps/hash_ASV.tsv >tree_taxonomy/taxonomy_filter.tsv

qiime tools import   --input-path tree_taxonomy/taxonomy_filter.tsv \
--output-path tree_taxonomy/taxonomy_filter.qza --type 'FeatureData[Taxonomy]' \
--input-format HeaderlessTSVTaxonomyFormat


mkdir -p tree_taxonomy/tree_qza
#筛选前50
sed -n '2,52p' tables_reps/table_filter.xls |cut -f1 > tables_reps/50_id
#qiime feature-table filter-features --i-table tables_reps/table.qza --o-filtered-table tables_reps/table.qza --m-metadata-file tables_reps/50_id
qiime feature-table  filter-seqs --i-data tables_reps/rep_seqs_filter.qza --o-filtered-data tables_reps/rep_seqs_filter_50.qza  --m-metadata-file tables_reps/50_id
#优势物种建树

if [ $tree_type == fasttree ];then
    qiime phylogeny align-to-tree-mafft-fasttree --p-n-threads "auto"  --i-sequences tables_reps/rep_seqs_filter_50.qza  \
    --o-alignment tree_taxonomy/tree_qza/aligned_rep_seq --o-masked-alignment tree_taxonomy/tree_qza/masked_aligned_rep_seqs_filtered.qza  \
    --o-tree tree_taxonomy/unrooted_tree.qza  --o-rooted-tree tree_taxonomy/rooted_tree.qza
else
    qiime phylogeny align-to-tree-mafft-raxml --p-n-threads "auto"  --i-sequences tables_reps/rep_seqs_filter_50.qza   \
    --o-alignment tree_taxonomy/tree_qza/aligned_rep_seq --o-masked-alignment tree_taxonomy/tree_qza/masked_aligned_rep_seqs_filtered.qza  \
    --o-tree tree_taxonomy/unrooted_tree.qza  --o-rooted-tree tree_taxonomy/rooted_tree.qza
fi

qiime feature-classifier classify-sklearn    --p-n-jobs -1 --i-classifier $db_file  --i-reads tables_reps/rep_seqs_filter.qza  --o-classification tree_taxonomy/taxonomy.qza



mkdir input_R
cp  tables_reps/table_filter.qza tree_taxonomy/taxonomy_filter.qza tree_taxonomy/unrooted_tree.qza input_R/

#矫正格式，作为qiime元数据导入
tail -n1 $meta_d|awk '{ for(i=2;i<=NF;i++){if($i~ /^[0-9.]+/){print"numeric"}else{print"categorical"}} }' |tr "\n" "\t"|xargs -i sed '2i#q2:types\t{}'  $meta_d|awk '(NR==1){for(i=1;i<=NF-1;i++){if($i!="barcode"){printf $i"\t"}else{a=i}};printf$NF"\n"}(NR>1){for(i=1;i<=NF-1;i++){if(a!=i){printf $i"\t"}};printf$NF"\n"}' > input_R/meta.tsv

mkdir -p pic_views/RDA_CCA
conda deactivate
#conda activate R4.0
source activate /PERSONALBIO196/Mb_std/miniconda3/envs/R4.0
Rscript all_plot.r
conda deactivate
