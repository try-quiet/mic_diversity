library(qiime2R)
library(tidyverse)
library(ggplot2)
library(ggrepel)
library(ggtree)
library(vegan)
library(ggvegan)
library(ggvenn)
library(plotrix)
library(RColorBrewer)
#定义rgb颜色,画花瓣图
ellipse_col <- c( '#BF5B1790', '#F0027F90', '#386CB090', '#FDC08690', '#BEAED490', '#7FC97F90', '#FB807290', '#66666690', '#B5B1B190', '#BF5B1750', '#F0027F50', '#386CB050', '#FDC08650', '#BEAED450', '#7FC97F50', '#FB807250', '#66666650', '#B5B1B150','#6181BD4E','#F348004E','#64A10E4E','#9300264E','#464E044E','#049a0b4E','#4E0C664E','#D000004E','#FF6C004E','#FF00FF4E','#c7475b4E','#00F5FF4E','#BDA5004E','#A5CFED4E','#f0301c4E','#2B8BC34E','#FDA1004E','#54adf54E','#CDD7E24E','#9295C14E')

qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
#处理后有73种差异还比较明显的颜色，基本够用
diff_col <- unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals))) 

#定义花瓣图画法
flower_plot <- function(ASV_table,ellipse_col) {
  num_asvs_0<-as.matrix(rowSums(ASV_table=="0")) #统计每行中，0出现个数
  num_core<-colSums(num_asvs_0=="0")#统计“0”出现0次的情况数，即为所有样品都有的asv数目
  sample_id<-colnames(ASV_table)
  num_in_sample<-colSums(ASV_table!="0")  

  par( bty = 'n', ann = F, xaxt = 'n', yaxt = 'n', mar = c(1,1,1,1))
  plot(c(0,10),c(0,10),type='n')
  n <- length(sample_id) 
  deg <- 360 / n
  res <- lapply(1:n, function(t){
    draw.ellipse(x = 5 + cos((90 + deg * (t - 1)) * pi / 180), y = 5 + sin((90 + deg * (t - 1)) * pi / 180),
                 col = ellipse_col[t],border = ellipse_col[t],a = 0.5, b = 2, angle = deg * (t - 1))
    text(x = 5 + 2.5 * cos((90+ deg * (t - 1)) * pi / 180),y = 5 + 2.5 * sin((90 + deg * (t - 1)) * pi / 180),
         num_in_sample[t])
    if (deg * (t - 1) < 180 && deg * (t - 1) > 0 ) {
      text(x = 5 + 3.3 * cos((90 + deg * (t - 1)) * pi / 180),y = 5 + 3.3 * sin((90 + deg * (t - 1)) * pi / 180),
           sample_id[t], srt = deg * (t - 1) - 90,adj = 1,cex = 1)
    } else {
      text(x = 5 + 3.3 * cos((90 + deg * (t - 1)) * pi / 180),y = 5 + 3.3 * sin((90 + deg * (t - 1)) * pi / 180),
           sample_id[t],srt = deg * (t - 1) + 90,adj = 0,cex = 1)
    }            
  })
  draw.circle(x = 5, y = 5, r = 1, col = 'white', border = NA) #画中心圆
  text(x = 5, y = 5, paste('Core:', num_core))
}

ASV_tab<-read_qza("input_R/table_filter.qza")$data
ASV_tab_rary<-rrarefy(t(ASV_tab),min(colSums(ASV_tab))) %>%t() %>%as.data.frame()
metadata<-read_q2metadata("input_R/meta.tsv") 
meta_group<-metadata[,1:2]
taxonomy<-read_qza("input_R/taxonomy_filter.qza")$data %>% parse_taxonomy()
taxonomy[is.na(taxonomy)]<-"unclassfiled" #空置填充
taxasums<-summarize_taxa(ASV_tab, taxonomy)$Class #纲水平统计

tree<-read_qza("input_R/unrooted_tree.qza")$data

#花瓣图
png('./pic_views/flower.png', width = 1200, height = 1200 , res = 200, units = 'px')
pic_flower<-flower_plot(ASV_table = ASV_tab, ellipse_col = ellipse_col)
dev.off()
#组间韦恩图
group_nums<-meta_group$group%>%unique()%>%length()
if (group_nums>1 & group_nums<5){
  gp_split <- split(meta_group$SampleID, meta_group$group)
  ASV_gp_num<-data.frame(lapply(gp_split, function(i) rowSums(as.data.frame(ASV_tab)[i] )))
  ASV_gp_char<-lapply(ASV_gp_num,function(x) rownames(ASV_gp_num)[x>0])
  ggvenn(ASV_gp_char,fill_color = diff_col)
  ggsave("./pic_views/venn.png", height=8, width=8.55, device="png") 
}
#ASV热图（纲水平）
pic_heat<-taxa_heatmap(taxasums, metadata,category = "group", "none")
ggsave("./pic_views/heatmap.png", height=3.86, width=8.55, device="png") 
#分类柱状图（纲水平）
pic_barplot<-taxa_barplot(taxasums, metadata,category = "group")
ggsave("./pic_views/barplot.png", height=3.86, width=8.55, device="png")

#建树
tax_50<-taxonomy[1:50,]
groupInfo<-split(row.names(tax_50),tax_50$Class)
tree <- groupOTU(tree,groupInfo)
pic_tree<-ggtree(tree, layout="circular",branch.length="none",aes(color=group))+
geom_tiplab(show.legend=FALSE)+
theme(legend.position = "right")+
labs(colour="Class")+
scale_colour_manual(values=diff_col)
ggsave("./pic_views/tree.png", height=8, width=8.55, device="png")


#稀释曲线

png('./pic_views/Dilution_curve.png', width = 1200, height = 1000,res = 200, units = 'px')
aaa<-rarecurve(t(ASV_tab_rary),step = 500,col=diff_col,label=FALSE,ylab ="obverse species",xaxs="i",yaxt="n",xaxt="n",lwd=2)
axis(2,tcl=0.1,las=1)
axis(1,tcl=0.1)
legend("topright",colnames(ASV_tab),col=diff_col,lty=1,lwd=2,cex=0.5)
dev.off()


#箱线图
# shannon<-diversity(t(ASV_tab_rary),"shannon") %>% as.data.frame()
# names(shannon)<-c("Shannon")
# simpson<-diversity(t(ASV_tab_rary),"simpson") %>% as.data.frame()
# names(simpson)<-c("Simpson")
# ace_chao<-estimateR(t(ASV_tab_rary) )%>%t()%>%as.data.frame()
# names(ace_chao)<-c("observed_species","Chao1","Chao1_se","Ace","Ace_se")
# div<-merge(meta_group,shannon,by.x="SampleID",by.y="row.names")%>%
#   merge(simpson,by.x="SampleID",by.y="row.names")%>%
#   merge(ace_chao,by.x="SampleID",by.y="row.names")
# # Plot
# diver_bax_plot<- function(df) {
#   name_div<-c("Shannon","Simpson","Chao1","Ace")
#   name_clor<-diff_col[1:4]
#   for (i in seq_along(name_div)) {
#     pic<-ggplot(df, aes_string("group",name_div[i]))+
#       stat_boxplot(geom="errorbar")+
#       geom_boxplot(fill=name_clor[i])+
#       geom_jitter(aes_string(fill="group",color="group"))
#     if (i==1){
#       pic_all<-pic
#     }else{
#       pic_all<-pic_all+pic
#     }
#   }
#   return(pic_all)
# }
shannon<-diversity(t(ASV_tab_rary),"shannon") %>% as.data.frame()
names(shannon)<-c("Shannon")

Pielou<-shannon/log(specnumber(t(ASV_tab_rary)))
names(Pielou)<-c("Pielou")

ace_chao<-estimateR(t(ASV_tab_rary) )%>%t()%>%as.data.frame()
names(ace_chao)<-c("observed_species","Chao","Chao1_se","Ace","Ace_se")
div<-merge(meta_group,shannon,by.x="SampleID",by.y="row.names")%>%
  merge(Pielou,by.x="SampleID",by.y="row.names")%>%
  merge(ace_chao,by.x="SampleID",by.y="row.names")
# Plot
diver_bax_plot<- function(df) {
  name_div<-c("Shannon","Pielou","Chao")
  name_clor<-diff_col[1:4]
  for (i in seq_along(name_div)) {
    pic<-ggplot(df, aes_string("group",name_div[i]))+
      stat_boxplot(geom="errorbar")+
      geom_boxplot(fill=name_clor[i])+
      geom_jitter(aes_string(fill="group",color="group"))
    if (i==1){
      pic_all<-pic
    }else{
      pic_all<-pic_all+pic
    }
  }
  return(pic_all)
}


pic_alpha<-diver_bax_plot(div)
ggsave("./pic_views/alpha_diversity.png",height=3.86, width=8.55,device="png")




#PCoA
asv_hell<-decostand(t(ASV_tab), method = 'hellinger') #标准化
pcoa <-vegdist(asv_hell,method = 'bray')%>% #计算矩阵
cmdscale(k = (ncol(ASV_tab) - 1), eig = TRUE)  #排序
pcoa_eig<-data.frame(scores(pcoa)[,1:2])#取坐标
pcoa_eig$SampleID<-rownames(pcoa_eig)#增加分组信息以映射
pcoa_eig<-merge(pcoa_eig,meta_group,by='SampleID')
ggplot(pcoa_eig, aes(Dim1, Dim2))+geom_point(aes(color = group),size=2)+
labs(x = "PCoA1", y ="PCoA2")+
theme(panel.grid = element_blank(), panel.background = element_rect(color = 'black', fill = 'transparent'))+
geom_vline(xintercept = 0, color = 'gray', size = 0.5) + geom_hline(yintercept = 0, color = 'gray', size = 0.5)+geom_label_repel(aes(Dim1,Dim2,label=SampleID,color=group),show.legend=FALSE)
ggsave("./pic_views/PCoA.png", height=8, width=8.55, device="png")

#RDa_CCA
if (ncol(metadata)>2 & ncol(metadata)<nrow(metadata)){
  dir.create("pic_views/RDA_CCA/")
  
  env<-metadata[,3:ncol(metadata)]%>%log10()
  row.names(env)<-metadata[,1]
  sink("pic_views/RDA_CCA/dca_o.txt") #dca根据第一列决定RDA还是CCA
  print(decorana(asv_hell))
  sink()
  #CCA
  rda_res<-rda(asv_hell~.,env,scale=FALSE)
  sam_point<-merge(meta_group,rda_res$CCA$u,by.x="SampleID",by.y="row.names")
  pic_rda<-autoplot(rda_res, arrows = FALSE,layers = c( "biplot"),geom = c("point") )+
  geom_point(data=sam_point,aes(RDA1,RDA2,colour=group))+labs(colour="group")
  ggsave("./pic_views/RDA_CCA/RDA.png", height=8, width=8.55, device="png")
  #CCA
  cca_res<-cca(asv_hell~.,env,scale=FALSE)
  sam_point_c<-merge(meta_group,cca_res$CCA$u,by.x="SampleID",by.y="row.names")
  pic_cca<-autoplot(cca_res, arrows = FALSE,layers = c("biplot"),geom = c("point"))+
  geom_point(data=sam_point_c,aes(x=CCA1,y=CCA2,colour=group))+labs(colour="group")
  ggsave("./pic_views/RDA_CCA/CCA.png", height=8, width=8.55, device="png")
  
  
  #以下代码似乎在Rtudio正常运行，但linux服务器绘图会出问题。。。
  #RDA
  # sam_info<-fortify(rda(asv_hell~.,env,scale=FALSE),display = "wa") #样品坐标
  # sam_info<-merge(sam_info,meta_group,by.x = "Label",by.y = "SampleID")
  # spc_info<-fortify(rda(asv_hell~.,env,scale=FALSE),display = "sp")#物种坐标
  # env_info<-fortify(rda(asv_hell~.,env,scale=FALSE),display = "bp") #环境因子
  # 
  # pic_rda<-ggplot(data=env_info,aes(RDA1,RDA2))+theme_bw()+
  #   geom_point(data=spc_info,size=1,aes(RDA1,RDA2),color="lemonchiffon2")+
  #   geom_point(data=sam_info,size=2,aes(RDA1,RDA2,color=group))+
  #   geom_segment(data=env_info,aes(x=0,y=0,xend=RDA1,yend=RDA2),arrow=arrow(length = unit(0.2,"cm")),colour="gray30")+
  #   geom_text_repel(data = env_info,color="black", aes(RDA1, RDA2, label =Label), size=3.5)
  # ggsave("./pic_views/RDA_CCA/RDA.png", height=8, width=9, device="png")
  # 
  # #CCA
  # sam_info<-fortify(cca(asv_hell~.,env,scale=FALSE),display = "wa") #样品坐标
  # sam_info<-merge(sam_info,meta_group,by.x = "Label",by.y = "SampleID")
  # spc_info<-fortify(cca(asv_hell~.,env,scale=FALSE),display = "sp")#物种坐标
  # env_info<-fortify(cca(asv_hell~.,env,scale=FALSE),display = "bp") #环境因子
  # pic_rda<-ggplot(data=env_info,aes(CCA1,CCA2))+theme_bw()+
  #   geom_point(data=spc_info,size=1,aes(CCA1,CCA2),color="lemonchiffon2")+
  #   geom_point(data=sam_info,size=2,aes(CCA1,CCA2,color=group))+
  #   geom_segment(data=env_info,aes(x=0,y=0,xend=CCA1,yend=CCA2),arrow=arrow(length = unit(0.2,"cm")),colour="gray30")+
  #   geom_text_repel(data = env_info,color="black", aes(CCA1, CCA2, label =Label), size=3.5)
  # ggsave("./pic_views/RDA_CCA/CCA.png", height=8, width=9, device="png")
  # 

}



